<!DOCTYPE html>
<html>
<head>
    <title>Error page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="resources/css/core.css" rel="stylesheet" media="screen"/>
</head>
<body>
<div class="container">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <c:set var="alertType" value="danger"/>
    <c:set var="alertMessage" value="${errorMessage}"/>
    <%@ include file="/WEB-INF/views/fragments/alert.jsp" %>
    <%@ include file="/WEB-INF/views/fragments/footer.jsp" %>