<div class="alert alert-dismissable alert-${alertType}">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <span>${alertMessage}</span>
</div>
