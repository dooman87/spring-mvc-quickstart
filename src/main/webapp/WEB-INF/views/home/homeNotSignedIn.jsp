<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="title" value="Welcome to the Spring MVC Quickstart application! Get started quickly by signing up!"/>
<%@ include file="/WEB-INF/views/fragments/header.jsp"%>
<div class="container">
    <div class="text-center">
        <h1>SpringMVC Login Application</h1>
        <p>
            <a href="signup"class="btn btn-success btn-lg">Sign up</a>
        </p>
    </div>
</div>
<%@ include file="/WEB-INF/views/fragments/footer.jsp"%>