<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "spring" uri = "http://www.springframework.org/tags" %>
<%@ taglib prefix = "sec" uri = "http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:message code="view.index.title" var="title"/>

<%@ include file="/WEB-INF/views/fragments/header.jsp"%>

<div class="container">
  <c:if test="${message != null}">
      <c:set var="alertType" value="${message.type}"/>
      <sec:authentication var="user" property="principal" />
      <spring:message code="${message.message}" var="alertMessage" arguments="${user.username}"/>
      <%@ include file="/WEB-INF/views/fragments/alert.jsp"%>
  </c:if>

  <table class="table">
    <thead>
      <tr>
        <th>Email</th>
        <th>Address</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach items="${accounts}" var="user">
        <tr>
          <td>${user.email}</td>
          <td>${user.address}</td>
        </tr>
      </c:forEach>
    </tbody>
  </table>

</div>
<%@ include file="/WEB-INF/views/fragments/footer.jsp"%>
