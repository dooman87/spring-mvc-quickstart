<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="title" value="Sign In"/>
<%@ include file="/WEB-INF/views/fragments/header.jsp"%>
<spring:hasBindErrors name="signupForm">
    <c:set var="formErrors" value="${errors}"/>
</spring:hasBindErrors>

<form class="form-narrow form-horizontal" method="post" action="j_spring_security_check">
    <c:if test="${param.error != null}">
        <!-- /* Show general error message when form contains errors */ -->
        <c:set var="alertType" value="danger"/>
        <c:set var="alertMessage" value="Sign in error. Please try again."/>
        <%@ include file="/WEB-INF/views/fragments/alert.jsp" %>
    </c:if>

    <fieldset>
        <legend>Please Sign In</legend>

        <div class="form-group">
            <label for="inputEmail" class="col-lg-2 control-label">Email</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="inputEmail" placeholder="Email" name="j_username" />
            </div>
        </div>
        <div class="form-group">
            <label  for="inputPassword" class="col-lg-2 control-label">Password</label>
            <div class="col-lg-10">
                <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="j_password"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="_spring_security_remember_me" /> Remember me
                    </label>
                </div>
            </div>
        </div>
        
        <input type="hidden"
            name="${_csrf.parameterName}"
            value="${_csrf.token}"/>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <button type="submit" class="btn btn-default">Sign in</button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <p>New here? <a href="signup">Sign Up</a></p>
            </div>
        </div>
    </fieldset>
</form>
<%@ include file="/WEB-INF/views/fragments/footer.jsp"%>