<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="title" value="Signup"/>
<%@ include file="/WEB-INF/views/fragments/header.jsp"%>

<spring:hasBindErrors name="signupForm">
    <c:set var="formErrors" value="${errors}"/>
</spring:hasBindErrors>

<form:form class="form-narrow form-horizontal" method="post" action="signup" commandName="signupForm" modelAttribute="signupForm">
    <spring:hasBindErrors name="signupForm">
      <!-- /* Show general error message when form contains errors */ -->
      <c:set var="alertType" value="danger"/>
      <c:set var="alertMessage" value="Form contains errors. Please try again."/>
      <%@ include file="/WEB-INF/views/fragments/alert.jsp" %>
    </spring:hasBindErrors>

    <fieldset>
        <legend>Please Sign Up</legend>

        <div class="form-group">
            <label for="email" class="col-lg-2 control-label">Email</label>
            <div class="col-lg-10 ${formErrors.hasFieldErrors('email') ? 'has-error' : ''}">
                <form:input class="form-control" id="email" placeholder="Email address" path="email" />
                <form:errors path="email" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-lg-2 control-label">Password</label>
            <div class="col-lg-10 ${formErrors.hasFieldErrors('password') ? 'has-error' : ''}">
                <form:password class="form-control" id="password" placeholder="Password" path="password"/>
                <form:errors path="password" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-lg-2 control-label">Address</label>
            <div class="col-lg-10 ${formErrors.hasFieldErrors('address') ? 'has-error' : ''}">
                <form:input class="form-control" id="address" placeholder="Address" path="address" />
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <button type="submit" class="btn btn-default">Sign up</button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <p>Already have an account? <a href="signin">Sign In</a></p>
            </div>
        </div>
        
    </fieldset>
</form:form>
<%@ include file="/WEB-INF/views/fragments/footer.jsp"%>