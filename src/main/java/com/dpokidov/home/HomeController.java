package com.dpokidov.home;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.dpokidov.account.Account;
import com.dpokidov.account.AccountRepository;

@Controller
public class HomeController {
    @Autowired
    private AccountRepository accountRepository;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Principal principal) {
		return principal != null ? "home/homeSignedIn" : "home/homeNotSignedIn";
	}

	@ModelAttribute("accounts")
	public List<Account> populateAccount() {
	    return accountRepository.getAccounts();
	}
}
