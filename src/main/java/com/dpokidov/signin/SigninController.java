package com.dpokidov.signin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dpokidov.account.AccountRepository;

@Controller
public class SigninController {
	@RequestMapping(value = "signin")
	public String signin() {
        return "signin/signin";
    }
}
